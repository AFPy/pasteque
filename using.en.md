# Paf'Py

## TL;DR

Use this `bash` function:

```bash
paf()
{
    curl https://p.afpy.org/ "${@/*/-F&=@&}"
}
```

it allows to send a file:

```bash
$ paf manage.py
- https://p.afpy.org/TaPa/manage.py
```

or many:

```bash
$ paf *.py
- https://p.afpy.org/d6Xd/admin.py
- https://p.afpy.org/d6Xd/context_processors.py
- https://p.afpy.org/d6Xd/__init__.py
- https://p.afpy.org/d6Xd/models.py
- https://p.afpy.org/d6Xd/urls.py
- https://p.afpy.org/d6Xd/utils.py
- https://p.afpy.org/d6Xd/views.py
```

A more complicated version is provided below.


## The projet

Pasteque is a FOSS project implemented by Alexandre Henriet around
2013 under the MIT license, around 2018 Julien Palard took over for
https://wyz.fr and finally around 2023 the AFPy took over for
https://p.afpy.org.

Source code is here: https://git.afpy.org/AFPy/pasteque


## Using with `curl`

All examples uses `curl`, feel free to adapt:

Using `multipart/form-data` HTTP requests it is possible to send a file:

```bash
$ curl https://p.afpy.org/ -Fmanage.py=@manage.py
- https://p.afpy.org/TaPa/manage.py
```

Or multiples files:

```bash
$ curl https://p.afpy.org/ -Fmanage.py=@manage.py -Frequirements.txt=@requirements.txt
- https://p.afpy.org/Nz2o/manage.py
- https://p.afpy.org/Nz2o/requirements.txt
```

The extension is used to determine syntax coloration to be used for each file.

File without extension, or ending with `.md` are interepreted as Markdown.

Files are always given back raw outside of a browser, typically via
`curl` or `wget`.


### Sending in a request body

It is possible to send a single file as a POST body:

```bash
$ cal | curl -XPOST --data-binary @- https://p.afpy.org/
- https://p.afpy.org/mo8X
```

Without a name, the file has no extension, and will be rendered as
Markdown, it may not be what you want.

In this case it is possible to override the syntax coloration using the `Content-Type` header:

```bash
$ cal | curl -XPOST -H "Content-Type: text/plain" --data-binary @- https://p.afpy.org/
- https://p.afpy.org/i3Y2/request.txt
```

### Choosing the URL

It is possible to use a `PUT` instead of a `POST` to choose the paste URL:

```bash
$ cal | curl -XPUT --data-binary @- https://p.afpy.org/cal
- https://p.afpy.org/cal
```

```bash
$ curl -XPUT --data-binary @manage.py https://p.afpy.org/manage.py
- https://p.afpy.org/manage.py
```


### Overwriting (updating) pasts

When sending a paste, you can assign it a secret using the `Authorization` header:

```bash
$ date | curl -XPUT --data-binary @- -H "Authorization: Secret supersecret" https://p.afpy.org/date
- https://p.afpy.org/date
```

This secret can now be used to update the same file:

```bash
$ date | curl -XPUT --data-binary @- -H "Authorization: Secret supersecret" https://p.afpy.org/date
- https://p.afpy.org/date
$ curl https://p.afpy.org/date
Tue Apr 25 06:22:45 PM CEST 2023
$ date | curl -XPUT --data-binary @- -H "Authorization: Secret supersecret" https://p.afpy.org/date
- https://p.afpy.org/date
$ curl https://p.afpy.org/date
Tue Apr 25 06:23:44 PM CEST 2023
```

The home page and the `robots.txt` file of https://p.afpy.org are handled using this method.


### Listing own pasts

All pastes sharing the same secret can be listed using the `/::/list/` URL:

```bash
$ curl https://p.afpy.org/::/list/ -H "Authorization: Secret supersecret"
| filename   |   size | URL                 | paste_time                | access_time               |   viewcount |
|------------|--------|---------------------|---------------------------|---------------------------|-------------|
|            |   5168 | https://p.afpy.org/ | 2023-04-25T16:03:59+00:00 | 2023-04-25T16:26:59+00:00 |           2 |
```


## Advanced usage

With this function (in which the secret have to be changed, I
 recommend you to use [pass](https://www.passwordstore.org/) but feel
 free to hardcode the password):

```bash
paf()
{
    local SECRET="$(pass paf)"
    local AUTH="-HAuthorization: Secret $SECRET"
    local INSTANCE="https://p.afpy.org/"

    if [[ $1 == "--list" ]]
    then
        curl "$AUTH" $INSTANCE::/list/
        return
    fi

    if [[ $# == 0 ]]
    then
        curl "$AUTH" "$INSTANCE" --data-binary @-
    else
        curl "$AUTH" "$INSTANCE" "${@/*/-F&=@&}"
    fi
}

code-block()
{
    printf '```%s\n' "$1"
    cat
    printf '```\n'
}
```

It is possible to:

- send one or many files,
- send from `stdin`,
- list own files.

The `code-block` function is helpfull to transform non-markdown to a
Markdown `code-block` while piping, like:

```bash
$ cal | code-block | paf
- https://p.afpy.org/WrmY
```

Without the `code-block` we'll get: https://p.afpy.org/aAkF ☹

If you want to share a complete folder, just paste the list back:

```
$ paf *.py | paf
- https://p.afpy.org/nnLR
```

Which gives:

```
$ curl https://p.afpy.org/nnLR
- https://p.afpy.org/fAqR/admin.py
- https://p.afpy.org/fAqR/context_processors.py
- https://p.afpy.org/fAqR/__init__.py
- https://p.afpy.org/fAqR/models.py
- https://p.afpy.org/fAqR/urls.py
- https://p.afpy.org/fAqR/utils.py
- https://p.afpy.org/fAqR/views.py
```

And, in a browser, as it's interpreted as Markdown, you can click the links.


## To get further

- [End-to-end encryption](https://p.afpy.org/e2e.md).
- [inline HTML in Markdown and the &lt;summary&gt; tag](https://p.afpy.org/summary.md).


## Note to self

To update this (french) page on p.afpy.org I use:

```bash
$ curl -H "Authorization: Secret $(pass paf)" -XPUT --data-binary @using.fr.md https://p.afpy.org/
```
