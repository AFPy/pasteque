# Paf’Py

Et PAF !

## TL;DR

Utilisez cette fonction `bash` :

```bash
paf()
{
    curl https://p.afpy.org/ "${@/*/-F&=@&}"
}
```

elle permet d’envoyer un fichier :

```bash
$ paf manage.py
- https://p.afpy.org/TaPa/manage.py
```

ou plusieurs :

```bash
$ paf *.py
- https://p.afpy.org/d6Xd/admin.py
- https://p.afpy.org/d6Xd/context_processors.py
- https://p.afpy.org/d6Xd/__init__.py
- https://p.afpy.org/d6Xd/models.py
- https://p.afpy.org/d6Xd/urls.py
- https://p.afpy.org/d6Xd/utils.py
- https://p.afpy.org/d6Xd/views.py
```

une version évoluée de cette fonction est fournie plus bas.


## Le projet

Pasteque est un projet libre et open-source développé par Alexandre
Henriet en 2013 sous licence MIT puis repris par Julien Palard en 2018 pour
https://wyz.fr et enfin par l’AFPy en 2023 pour https://p.afpy.org.

Le code source est disponible ici : https://git.afpy.org/AFPy/pasteque


## Utilisation avec `curl`

Tous les exemples sont rédigés avec `curl`, libre à vous d’adapter à
votre usage.

En utilisant des requêtes `multipart/form-data` il est possible
d’envoyer un fichier :

```bash
$ curl https://p.afpy.org/ -Fmanage.py=@manage.py
- https://p.afpy.org/TaPa/manage.py
```

ou plusieurs fichiers en même temps :

```bash
$ curl https://p.afpy.org/ -Fmanage.py=@manage.py -Frequirements.txt=@requirements.txt
- https://p.afpy.org/Nz2o/manage.py
- https://p.afpy.org/Nz2o/requirements.txt
```

C’est l’extension dans le nom du fichier qui permet de choisir la
coloration syntaxique pour chaque fichier.

Les fichiers sans extension et les fichiers `.md` sont interprétés en
Markdown lors de l’affichage dans un navigateur.

Les fichiers sont toujours rendus tels quels lorsqu’ils sont récupérés
hors d’un navigateur, typiquement via `wget` ou `curl`.


### Envoyer dans le corps d’une requête

Il est possible de coller un unique fichier via le corps d’une requête POST :

```bash
$ cal | curl -XPOST --data-binary @- https://p.afpy.org/
- https://p.afpy.org/mo8X
```

N’ayant pas d’extension, il est interprété en Markdown, ce n’est pas toujours souhaitable.

Dans ce cas, il est possible de choisir la coloration syntaxique via l’entête `Content-Type` :

```bash
$ cal | curl -XPOST -H "Content-Type: text/plain" --data-binary @- https://p.afpy.org/
- https://p.afpy.org/i3Y2/request.txt
```

### Choisir l’URL

Il est possible d’utiliser la méthode `PUT` sur une `URL` :

```bash
$ cal | curl -XPUT --data-binary @- https://p.afpy.org/cal
- https://p.afpy.org/cal
```

```bash
$ curl -XPUT --data-binary @manage.py https://p.afpy.org/manage.py
- https://p.afpy.org/manage.py
```


### Écraser un paste existant

Lors du premier envoi d’un paste, vous pouvez lui affecter un secret via l’entête HTTP `Authorization` :

```bash
$ date | curl -XPUT --data-binary @- -H "Authorization: Secret supersecret" https://p.afpy.org/date
- https://p.afpy.org/date
```

Ainsi vous pouvez mettre à jour ce `paste` en utilisant la même URL et le même secret :

```bash
$ date | curl -XPUT --data-binary @- -H "Authorization: Secret supersecret" https://p.afpy.org/date
- https://p.afpy.org/date
$ curl https://p.afpy.org/date
Tue Apr 25 06:22:45 PM CEST 2023
$ date | curl -XPUT --data-binary @- -H "Authorization: Secret supersecret" https://p.afpy.org/date
- https://p.afpy.org/date
$ curl https://p.afpy.org/date
Tue Apr 25 06:23:44 PM CEST 2023
```

La page d’accueil, ainsi que le fichier `robots.txt` sont maintenus de cette manière la.


### Lister ses envois

Tous les envois effectiés avec le même secret peuvent être listés via l’URL `/::/list/` :

```bash
$ curl https://p.afpy.org/::/list/ -H "Authorization: Secret supersecret"
| filename   |   size | URL                 | paste_time                | access_time               |   viewcount |
|------------|--------|---------------------|---------------------------|---------------------------|-------------|
|            |   5168 | https://p.afpy.org/ | 2023-04-25T16:03:59+00:00 | 2023-04-25T16:26:59+00:00 |           2 |
```


## Utilisation avancée

Avec cette fonction (dont le secret doit être personalisé, je vous
conseille d’utiliser [pass](https://www.passwordstore.org/) pour ça,
mais libre à vous d’écrire le mot de passe en clair dans votre
`~/.bashrc`) :

```bash
paf()
{
    local SECRET="$(pass paf)"
    local AUTH="-HAuthorization: Secret $SECRET"
    local INSTANCE="https://p.afpy.org/"

    if [[ $1 == "--list" ]]
    then
        curl "$AUTH" $INSTANCE::/list/
        return
    fi

    if [[ $# == 0 ]]
    then
        curl "$AUTH" "$INSTANCE" --data-binary @-
    else
        curl "$AUTH" "$INSTANCE" "${@/*/-F&=@&}"
    fi
}

code-block()
{
    printf '```%s\n' "$1"
    cat
    printf '```\n'
}
```

Il est possible :

- d’envoyer un ou plusieurs fichiers,
- d’envoyer `stdin`,
- de lister ses envois.

La fonction `code-block` sert à transformer, dans un pipe, un flux de
texte en block de code Markdown, c’est typiquement utile lorsqu’on
envoie quelque chose qui n’est vraiment pas du Markdown via `stdin`,
comme :

```bash
$ cal | code-block | paf
- https://p.afpy.org/WrmY
```

Sans quoi on obtiendrait : https://p.afpy.org/aAkF ☹

Si vous voulez partager un dossier complet, vous pouvez simplement
envoyer la liste des URL qui vous est renvoyée :

```
$ paf *.py | paf
- https://p.afpy.org/nnLR
```

Ce qui donne :

```
$ curl https://p.afpy.org/nnLR
- https://p.afpy.org/fAqR/admin.py
- https://p.afpy.org/fAqR/context_processors.py
- https://p.afpy.org/fAqR/__init__.py
- https://p.afpy.org/fAqR/models.py
- https://p.afpy.org/fAqR/urls.py
- https://p.afpy.org/fAqR/utils.py
- https://p.afpy.org/fAqR/views.py
```

Et dans un navigateur, c’est rendu via Markdown, les liens sont donc cliquables.


## Pour aller plus loin

- [Chiffrement de bout en bout](https://p.afpy.org/e2e.md).
- [HTML inline dans du Markdown, la balise &lt;summary&gt;](https://p.afpy.org/summary.md).


## Note à moi-même

Pour mettre à jour cette page, j’utilise :

```bash
$ curl -H "Authorization: Secret $(pass paf)" -XPUT --data-binary @using.fr.md https://p.afpy.org/
```
