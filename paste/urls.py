from django.urls import path, re_path
from django.views.static import serve

from paste import views
from webtools import settings

urlpatterns = [
    path("", views.IndexView.as_view(), name="index"),
    path("::/static/<path>", serve, {"document_root": settings.STATIC_ROOT}),
    path("::/list/", views.ListView.as_view()),
    re_path(r"^(?!::)(?P<path>.*)$", views.PasteView.as_view(), name="paste"),
]
