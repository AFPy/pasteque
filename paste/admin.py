from django.contrib import admin

from .models import Paste


@admin.register(Paste)
class PasteAdmin(admin.ModelAdmin):
    list_display = ("paste_time", "filename", "slug", "viewcount")
    readonly_fields = (
        "slug",
        "size",
        "paste_time",
        "access_time",
        "viewcount",
        "auth",
    )
    fields = (
        (
            "slug",
            "size",
        ),
        "auth",
        (
            "paste_time",
            "access_time",
            "viewcount",
        ),
        "filename",
        "content",
    )
