# Paf'Pastèque

## About

**Pasteque** is a free and open source (MIT License) **Pastebin like**
application written in Python/Django.

An instance can be found at: [p.afpy.org](https://p.afpy.org).  The AFPy
instance is installed by ansible, the role is available here:
[https://git.afpy.org/AFPy/infra/src/branch/main/roles/pasteque](https://git.afpy.org/AFPy/infra/src/branch/main/roles/pasteque).


## Features

- Support any database supported by Django (Sqlite3, MySQL, PostgreSQL, Oracle, ...)
- Syntax highlighting for a bunch of languages using Pygments.
- Rendering of Markdown as HTML.
- Easy paste from command line or any programming language.


## Using

See [using.en.md](using.en.md) or [using.fr.md](using.fr.md).

## FAQ

### Where's the homepage?

This pastebin has no homepage: its homepage is a paste like any
other. So to create one for your instance, just paste something to it, like:

    $ curl localhost:8000 -XPUT -H "Authorization: Secret supersecret" --data-binary @using.fr.md

The `Authorization` allows you to update the paste by uploading it again.


### I want a robots.txt

Just past it:

    $ printf "User-agent: *\nDisallow: /\n" | curl localhost:8000/robots.txt -XPUT -H "Authorization: Secret supersecret" --data-binary @-

It'll work as `.txt` files are not pretty printed.


### Where's the admin?

The admin is hosted as `/::/admin/`. As almost any URL can be used by pastes, we "burry" the admin here.

And no paste can start with `::`.


### What's this Authorization header?

By providing a secret in the `Authorization` header, one can edit its
pasts by `PUT`ting to it, and list all its pastes by querying:

    curl localhost:8000/::/list/ -H "Authorization: Secret supersecret"


## Running Pasteque

In a [venv](https://docs.python.org/3/library/venv.html), install the requirements:

    pip install -r requirements.txt

In a `local_settings.py` file, at the root of the repo:

    DISPLAY_NAME = 'YourCompany-Paste'
    COMPRESS_ENABLED = True
    DEBUG = False
    SECRET_KEY = 'fill_a_secret_key_here'
    ALLOWED_HOSTS = ['localhost', '127.0.0.1', 'paste.henriet.eu']

Create the database:

    ./manage.py migrate

If you're in production collect static files:

    ./manage.py collectstatic

Run it:

    ./manage.py runserver
