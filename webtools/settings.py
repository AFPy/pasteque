import os
import django

### Useful constants

DJANGO_ROOT = os.path.dirname(os.path.realpath(django.__file__))
SITE_ROOT = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

### Customize/configure Pasteque

DISPLAY_NAME = "Pasteque"
COMPRESS_ENABLED = False
SECRET_KEY = "change_me"
ALLOWED_HOSTS = ["localhost", "127.0.0.1"]
TIME_ZONE = "Europe/Brussels"
LANGUAGE_CODE = "fr-FR"
DEBUG = True
TEMPLATE_DEBUG = DEBUG
ADMINS = (("user", "user@hostname.domain"),)
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        "NAME": os.path.join(SITE_ROOT, "webtools.sqlite3"),
        # The following settings are not used with sqlite3:
        "USER": "",
        "PASSWORD": "",
        "HOST": "",  # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        "PORT": "",  # Set to empty string for default.
    }
}
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"
### End of customisation

APP_NAME = "Pasteque"
APP_VERSION = "v0.1"
SITE_ID = 1
MANAGERS = ADMINS
USE_TZ = True
MEDIA_URL = ""
COMPRESS_ROOT = os.path.join(SITE_ROOT, "static")
MEDIA_ROOT = os.path.join(SITE_ROOT, "assets")
STATIC_ROOT = os.path.join(SITE_ROOT, "static")
STATIC_URL = "/::/static/"
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "compressor.finders.CompressorFinder",
)
TEMPLATE_LOADERS = (
    "django.template.loaders.filesystem.Loader",
    "django.template.loaders.app_directories.Loader",
)

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "paste.context_processors.app_details",
            ],
        },
    },
]

MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
]

ROOT_URLCONF = "webtools.urls"
WSGI_APPLICATION = "webtools.wsgi.application"
INSTALLED_APPS = (
    "django.contrib.staticfiles",
    "django.contrib.contenttypes",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.messages",
    "django.contrib.sessions",
    "compressor",
    "paste",
)

PASTE = {
    "has_meta_table": False,
    "max_characters": 100000,
    "default_language": "Python",
}

try:
    from local_settings import *
except ImportError:
    pass
